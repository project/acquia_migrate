<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.4.0-dev db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('history', array(
  'fields' => array(
    'uid' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
    'nid' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
      'unsigned' => TRUE,
    ),
    'timestamp' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'default' => '0',
    ),
  ),
  'primary key' => array(
    'uid',
    'nid',
  ),
  'indexes' => array(
    'nid' => array(
      'nid',
    ),
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('history')
->fields(array(
  'uid',
  'nid',
  'timestamp',
))
->values(array(
  'uid' => '1',
  'nid' => '1',
  'timestamp' => '1652348160',
))
->values(array(
  'uid' => '1',
  'nid' => '2',
  'timestamp' => '1652348158',
))
->values(array(
  'uid' => '1',
  'nid' => '3',
  'timestamp' => '1652348153',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}