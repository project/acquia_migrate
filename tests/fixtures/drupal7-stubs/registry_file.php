<?php
// phpcs:ignoreFile
/**
 * @file
 * A database agnostic dump for testing purposes.
 *
 * This file was generated by the Drupal 9.4.0-dev db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();
// Ensure any tables with a serial column with a value of 0 are created as
// expected.
if ($connection->databaseType() === 'mysql') {
  $sql_mode = $connection->query("SELECT @@sql_mode;")->fetchField();
  $connection->query("SET sql_mode = '$sql_mode,NO_AUTO_VALUE_ON_ZERO'");
}

$connection->schema()->createTable('registry_file', array(
  'fields' => array(
    'filename' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '255',
    ),
    'hash' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '64',
    ),
  ),
  'primary key' => array(
    'filename',
  ),
  'mysql_character_set' => 'utf8',
));

$connection->insert('registry_file')
->fields(array(
  'filename',
  'hash',
))
->values(array(
  'filename' => 'includes/actions.inc',
  'hash' => 'f36b066681463c7dfe189e0430cb1a89bf66f7e228cbb53cdfcd93987193f759',
))
->values(array(
  'filename' => 'includes/ajax.inc',
  'hash' => '8d5ebead219c48d5929ee6a5a178a331471ee6ceb38653094514c952457eaebd',
))
->values(array(
  'filename' => 'includes/archiver.inc',
  'hash' => 'bdbb21b712a62f6b913590b609fd17cd9f3c3b77c0d21f68e71a78427ed2e3e9',
))
->values(array(
  'filename' => 'includes/authorize.inc',
  'hash' => '3eb984facfe9e0228e4d01ece6345cf33dfcd2fcc9c291b15f2e4f782a6029a9',
))
->values(array(
  'filename' => 'includes/batch.inc',
  'hash' => '756b66e69a05b74629dee0ff175385813b27eb635aa49380edd4a65532998825',
))
->values(array(
  'filename' => 'includes/batch.queue.inc',
  'hash' => '554b2e92e1dad0f7fd5a19cb8dff7e109f10fbe2441a5692d076338ec908de0f',
))
->values(array(
  'filename' => 'includes/bootstrap.inc',
  'hash' => 'cd5233bdbdb8cd32c474be5f9626a160b9e0a0f94e2526c0bf26bbece88dc2ac',
))
->values(array(
  'filename' => 'includes/cache-install.inc',
  'hash' => 'e7ed123c5805703c84ad2cce9c1ca46b3ce8caeeea0d8ef39a3024a4ab95fa0e',
))
->values(array(
  'filename' => 'includes/cache.inc',
  'hash' => '033c9bf2555dba29382b077f78cc00c82fd7f42a959ba31b710adddf6fdf24fe',
))
->values(array(
  'filename' => 'includes/common.inc',
  'hash' => '20eeb525bde84ef108196f820db29f73ade88c9a17f9c3cad073389f8db95e4b',
))
->values(array(
  'filename' => 'includes/database/database.inc',
  'hash' => '9f79732882753fda10b4c0a3fc9fe1cb7f4cdacef743a28664d8af6a855ac8b7',
))
->values(array(
  'filename' => 'includes/database/log.inc',
  'hash' => '9feb5a17ae2fabcf26a96d2a634ba73da501f7bcfc3599a693d916a6971d00d1',
))
->values(array(
  'filename' => 'includes/database/mysql/database.inc',
  'hash' => 'fb808762239838f920ffeb74a89db5894fb46131d8bb4c65a0caff82358562c6',
))
->values(array(
  'filename' => 'includes/database/mysql/install.inc',
  'hash' => '6ae316941f771732fbbabed7e1d6b4cbb41b1f429dd097d04b3345aa15e461a0',
))
->values(array(
  'filename' => 'includes/database/mysql/query.inc',
  'hash' => 'cddf695f7dbd483591f93af805e7118a04eac3f21c0105326642c6463587670c',
))
->values(array(
  'filename' => 'includes/database/mysql/schema.inc',
  'hash' => 'c34aa7b7d2cb4662965497ff86f242224116bbd9b72ca6287c12039a65feb72e',
))
->values(array(
  'filename' => 'includes/database/pgsql/database.inc',
  'hash' => 'f53cb5779d702dde7aefacf8ee25d67cac48d3b94cd8d64057d42c61c5713526',
))
->values(array(
  'filename' => 'includes/database/pgsql/install.inc',
  'hash' => '39587f26a9e054afaab2064d996af910f1b201ef1c6b82938ef130e4ff8c6aab',
))
->values(array(
  'filename' => 'includes/database/pgsql/query.inc',
  'hash' => 'bb04ae9239c2179aeb3ef0a55596ee5ae0ddfb5dfd701896c41bf8c42a62280b',
))
->values(array(
  'filename' => 'includes/database/pgsql/schema.inc',
  'hash' => '2d3f8e16b296e428c5f98d2942cda4c16a7e60fad8f1227f773533b164436f0d',
))
->values(array(
  'filename' => 'includes/database/pgsql/select.inc',
  'hash' => '1e509bc97c58223750e8ea735145b316827e36f43c07b946003e41f5bca23659',
))
->values(array(
  'filename' => 'includes/database/prefetch.inc',
  'hash' => '69a77926494328cd2e277dc3efd244d2a33d4946b14a2764572f582fdb2e68bd',
))
->values(array(
  'filename' => 'includes/database/query.inc',
  'hash' => 'e5e11f41bd65fdbd6bb931048705515ff2943d7c0f1492ca2efe10e667e2777b',
))
->values(array(
  'filename' => 'includes/database/schema.inc',
  'hash' => 'da9d48f26c3a47a91f1eb2fa216e9deab2ec42ba10c76039623ce7b6bc984a06',
))
->values(array(
  'filename' => 'includes/database/select.inc',
  'hash' => '2d52f4d35dcc9aba6eb5e13f31d485837ab48536e47751a7865ed3f5e98139ce',
))
->values(array(
  'filename' => 'includes/database/sqlite/database.inc',
  'hash' => '22e80c5a02c143eace3628e196dded78552e6f2889d1989d052e2a37f46e7f0f',
))
->values(array(
  'filename' => 'includes/database/sqlite/install.inc',
  'hash' => '6620f354aa175a116ba3a0562c980d86cc3b8b481042fc3cc5ed6a4d1a7a6d74',
))
->values(array(
  'filename' => 'includes/database/sqlite/query.inc',
  'hash' => '5d4dc3ac34cb2dbc0293471e85e37c890da3da6cd8c0c540c6f33313e4c0cbe9',
))
->values(array(
  'filename' => 'includes/database/sqlite/schema.inc',
  'hash' => '7bf3af0a255f374ba0c5db175548e836d953b903d31e1adb1e4d3df40d6fdb98',
))
->values(array(
  'filename' => 'includes/database/sqlite/select.inc',
  'hash' => '8d1c426dbd337733c206cce9f59a172546c6ed856d8ef3f1c7bef05a16f7bf68',
))
->values(array(
  'filename' => 'includes/date.inc',
  'hash' => '1de2c25e3b67a9919fc6c8061594442b6fb2cdd3a48ddf1591ee3aa98484b737',
))
->values(array(
  'filename' => 'includes/entity.inc',
  'hash' => 'f06b508f93e72ba70f979d8391be57662c018a03a32fac0a6d3baa752740133d',
))
->values(array(
  'filename' => 'includes/errors.inc',
  'hash' => '577e66843ac3ac0d6cb9d0808ff48d9e83acad0faeffad5dfe1ba09bf2631b97',
))
->values(array(
  'filename' => 'includes/file.inc',
  'hash' => 'cdf4a759fd1f20c3b2a3128765d89d1e44c0bf41ccee05cc7e7d8dccb91988bc',
))
->values(array(
  'filename' => 'includes/file.mimetypes.inc',
  'hash' => '33266e837f4ce076378e7e8cef6c5af46446226ca4259f83e13f605856a7f147',
))
->values(array(
  'filename' => 'includes/file.phar.inc',
  'hash' => '544df23f736ce49f458033d6515a301a8ca1c7a7d1bfd3f388caef910534abb3',
))
->values(array(
  'filename' => 'includes/filetransfer/filetransfer.inc',
  'hash' => '48a9cbcd6651a7fbf9fca9707ebc9fc79be36c33cd2f8c272b375123ce3f4dc3',
))
->values(array(
  'filename' => 'includes/filetransfer/ftp.inc',
  'hash' => '51eb119b8e1221d598ffa6cc46c8a322aa77b49a3d8879f7fb38b7221cf7e06d',
))
->values(array(
  'filename' => 'includes/filetransfer/local.inc',
  'hash' => '7cbfdb46abbdf539640db27e66fb30e5265128f31002bd0dfc3af16ae01a9492',
))
->values(array(
  'filename' => 'includes/filetransfer/ssh.inc',
  'hash' => '92f1232158cb32ab04cbc93ae38ad3af04796e18f66910a9bc5ca8e437f06891',
))
->values(array(
  'filename' => 'includes/form.inc',
  'hash' => 'ebf069dd1f75e76d6b9ff5901ee43ac3cecf06e44efd3628cde98b837c05df03',
))
->values(array(
  'filename' => 'includes/graph.inc',
  'hash' => '8e0e313a8bb33488f371df11fc1b58d7cf80099b886cd1003871e2c896d1b536',
))
->values(array(
  'filename' => 'includes/image.inc',
  'hash' => 'bcdc7e1599c02227502b9d0fe36eeb2b529b130a392bc709eb737647bd361826',
))
->values(array(
  'filename' => 'includes/install.core.inc',
  'hash' => 'b6f3e5d9bd4154f840253e34aed131bb401deb4fcb3421b379851231b8b4c149',
))
->values(array(
  'filename' => 'includes/install.inc',
  'hash' => 'dc7b5c97803df3e8e80e62984fe820de53ebc4141b645f66f6344f51ef4d5b19',
))
->values(array(
  'filename' => 'includes/iso.inc',
  'hash' => '09f14cce40153fa48e24a7daa44185c09ec9f56a638b5e56e9390c67ec5aaec8',
))
->values(array(
  'filename' => 'includes/json-encode.inc',
  'hash' => '02a822a652d00151f79db9aa9e171c310b69b93a12f549bc2ce00533a8efa14e',
))
->values(array(
  'filename' => 'includes/language.inc',
  'hash' => '4e08f30843a7ccaeea5c041083e9f77d33d57ff002f1ab4f66168e2c683ce128',
))
->values(array(
  'filename' => 'includes/locale.inc',
  'hash' => 'ca50acc0780cbffeca17f99a0997f91b8b9402f0eec1898c3122e1d73664d01d',
))
->values(array(
  'filename' => 'includes/lock.inc',
  'hash' => 'a181c8bd4f88d292a0a73b9f1fbd727e3314f66ec3631f288e6b9a54ba2b70fa',
))
->values(array(
  'filename' => 'includes/mail.inc',
  'hash' => 'a7bef724e057f7410e42c8f33b00c9a0246a2ca2e856a113c9e20eecc49fc069',
))
->values(array(
  'filename' => 'includes/menu.inc',
  'hash' => '9cbc6636d5c5f9c681eea9fd9c09973e2e29b66bca38420883b657f9e1c0800a',
))
->values(array(
  'filename' => 'includes/module.inc',
  'hash' => '943626f94bc69e95e36fde030475d57893f3296f0f8df461e2ee9f122dd37473',
))
->values(array(
  'filename' => 'includes/pager.inc',
  'hash' => '7d8d827eb2baace7031a02fd4b15a5e684928cd8345f878dd707adce11f93bd2',
))
->values(array(
  'filename' => 'includes/password.inc',
  'hash' => 'fd9a1c94fe5a0fa7c7049a2435c7280b1d666b2074595010e3c492dd15712775',
))
->values(array(
  'filename' => 'includes/path.inc',
  'hash' => 'acfd48f5582893af86cbb5ccf331ddb43bbf2671e879e5424a21c928d06d949f',
))
->values(array(
  'filename' => 'includes/registry.inc',
  'hash' => '2067cc87973e7af23428d3f41b8f8739d80092bc3c9e20b5a8858e481d03f22c',
))
->values(array(
  'filename' => 'includes/request-sanitizer.inc',
  'hash' => '770e8ece7b53d13e2b5ef99da02adb9a3d18071c6cd29eb01af30927cf749a73',
))
->values(array(
  'filename' => 'includes/session.inc',
  'hash' => '9981d139191b6a983f837e867058a376b62ae7cf5df607aee29e3e322a927b50',
))
->values(array(
  'filename' => 'includes/stream_wrappers.inc',
  'hash' => '6e63084fbf1f09d69bfc1b84ac9e769f204819df0d446a9a6b0276d6d4590ede',
))
->values(array(
  'filename' => 'includes/tablesort.inc',
  'hash' => '2d88768a544829595dd6cda2a5eb008bedb730f36bba6dfe005d9ddd999d5c0f',
))
->values(array(
  'filename' => 'includes/theme.inc',
  'hash' => 'ae46daba6419ca613bc6a08ba4d7f9bbab9b19889937099d2e4c1737e9e7b2df',
))
->values(array(
  'filename' => 'includes/theme.maintenance.inc',
  'hash' => '39f068b3eee4d10a90d6aa3c86db587b6d25844c2919d418d34d133cfe330f5a',
))
->values(array(
  'filename' => 'includes/token.inc',
  'hash' => '5e7898cd78689e2c291ed3cd8f41c032075656896f1db57e49217aac19ae0428',
))
->values(array(
  'filename' => 'includes/unicode.entities.inc',
  'hash' => '2b858138596d961fbaa4c6e3986e409921df7f76b6ee1b109c4af5970f1e0f54',
))
->values(array(
  'filename' => 'includes/unicode.inc',
  'hash' => '89636ce5847340fd19be319839b4203b0d4bbc3487973413d6de9b5f6f839222',
))
->values(array(
  'filename' => 'includes/update.inc',
  'hash' => '25c30f1e61ef9c91a7bdeb37791c2215d9dc2ae07dba124722d783ca31bb01e7',
))
->values(array(
  'filename' => 'includes/updater.inc',
  'hash' => 'd2da0e74ed86e93c209f16069f3d32e1a134ceb6c06a0044f78e841a1b54e380',
))
->values(array(
  'filename' => 'includes/utility.inc',
  'hash' => '3458fd2b55ab004dd0cc529b8e58af12916e8bd36653b072bdd820b26b907ed5',
))
->values(array(
  'filename' => 'includes/xmlrpc.inc',
  'hash' => 'ea24176ec445c440ba0c825fc7b04a31b440288df8ef02081560dc418e34e659',
))
->values(array(
  'filename' => 'includes/xmlrpcs.inc',
  'hash' => '925c4d5bf429ad9650f059a8862a100bd394dce887933f5b3e7e32309a51fd8e',
))
->values(array(
  'filename' => 'modules/block/block.test',
  'hash' => '40d9de00589211770a85c47d38c8ad61c598ec65d9332128a882eb8750e65a16',
))
->values(array(
  'filename' => 'modules/dblog/dblog.test',
  'hash' => '79ba7991c3f40f9241e9a03ffa43faf945c82658ca9b52ec62bd13bd80f41269',
))
->values(array(
  'filename' => 'modules/field/field.attach.inc',
  'hash' => '2df4687b5ec078c4893dc1fea514f67524fd5293de717b9e05caf977e5ae2327',
))
->values(array(
  'filename' => 'modules/field/field.info.class.inc',
  'hash' => '31deca748d873bf78cc6b8c064fdecc5a3451a9d2e9a131bc8c204905029e31f',
))
->values(array(
  'filename' => 'modules/field/field.module',
  'hash' => '48b5b83f214a8d19e446f46c5d7a1cd35faa656ccb7b540f9f02462a440cacdd',
))
->values(array(
  'filename' => 'modules/field/modules/field_sql_storage/field_sql_storage.test',
  'hash' => '6a5af7ced221d48e06395a7d99053ed462b9f1fe747320f3b91bdafd0027e2f6',
))
->values(array(
  'filename' => 'modules/field/modules/options/options.test',
  'hash' => '1b30956b6f46840ccb41b99bda08f328172f008f1fb4164c65fe9e4047fffa5f',
))
->values(array(
  'filename' => 'modules/field/modules/text/text.test',
  'hash' => '5c28b9da26417d2ed8a169850989c0b59f2b188a0161eb58e2b87c67994d602d',
))
->values(array(
  'filename' => 'modules/field/tests/field.test',
  'hash' => '57564727e284c7936821430d87ea9f4fd293ce53969ec5d6c12673c9609fa90f',
))
->values(array(
  'filename' => 'modules/filter/filter.test',
  'hash' => 'b8aa5e6b832422c6ad5fe963898ec9526c814614f27ecccb67107ce194997d6a',
))
->values(array(
  'filename' => 'modules/node/node.module',
  'hash' => 'a0431f275b291779ffd1061d7d98b6942106235350b807828e94c6929ad04a41',
))
->values(array(
  'filename' => 'modules/node/node.test',
  'hash' => 'f558b8453b6b90a0933d9901df6d09816753fda086d1d61d48c1232cb40eb26a',
))
->values(array(
  'filename' => 'modules/system/system.archiver.inc',
  'hash' => '05caceec7b3baecfebd053959c513f134a5ae4070749339495274a81bebb904a',
))
->values(array(
  'filename' => 'modules/system/system.mail.inc',
  'hash' => 'd2f4fca46269981db5edb6316176b7b8161de59d4c24c514b63fe3c536ebb4d6',
))
->values(array(
  'filename' => 'modules/system/system.queue.inc',
  'hash' => 'a77a5913d84368092805ac551ca63737c1d829455504fcccb95baa2932f28009',
))
->values(array(
  'filename' => 'modules/system/system.tar.inc',
  'hash' => '2dd9560bddab659f0379ef9eca095fc65a364128420d9d9e540ef81ca649a7d6',
))
->values(array(
  'filename' => 'modules/system/system.test',
  'hash' => 'e09f35f77b2be00ee7256dee45a8c57da55114a0980a104ebafd141a5452d62e',
))
->values(array(
  'filename' => 'modules/system/system.updater.inc',
  'hash' => '9433fa8d39500b8c59ab05f41c0aac83b2586a43be4aa949821380e36c4d3c48',
))
->values(array(
  'filename' => 'modules/user/user.module',
  'hash' => 'cdb344c247e56c78ccd740f0807267c803ceba004a07784eda9a656ec64fef21',
))
->values(array(
  'filename' => 'modules/user/user.test',
  'hash' => '5ca8c4ac5f137b587687ad1d1a224454fc18f4fab09bb0676ceb36d99997afdf',
))
->values(array(
  'filename' => 'sites/all/modules/references/node_reference/node_reference.test',
  'hash' => '5746afcbdfe47cfe75cce38c9690759cc2ab762bf2ae88d560aa0634dc671076',
))
->values(array(
  'filename' => 'sites/all/modules/references/views/references_handler_argument.inc',
  'hash' => '8548d8cd4676fc24bb26d0cf1a81640bbb8e8528c4b5a49195ff6c3d1fc53c41',
))
->values(array(
  'filename' => 'sites/all/modules/references/views/references_handler_relationship.inc',
  'hash' => '8fbf980fc3c9300ccb95e5efc4fa402233e4c9d4ed7a6d26b70264e37008c6e4',
))
->values(array(
  'filename' => 'sites/all/modules/references/views/references_plugin_display.inc',
  'hash' => 'eba22fcf16dff09cebd7f0864e8c7a51e1d1041e048dfda6160f6f7836741857',
))
->values(array(
  'filename' => 'sites/all/modules/references/views/references_plugin_row_fields.inc',
  'hash' => '061ea853c8d82b60aa472f23fdbe050e089d52ea0ede9567ecb0fc21b0135f5b',
))
->values(array(
  'filename' => 'sites/all/modules/references/views/references_plugin_style.inc',
  'hash' => 'b1761b3fdcb1fed613799011f148358b2a618d3f32126ca1c0465191d9885f74',
))
->execute();

// Reset the SQL mode.
if ($connection->databaseType() === 'mysql') {
  $connection->query("SET sql_mode = '$sql_mode'");
}