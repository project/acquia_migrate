<?php

namespace Drupal\Tests\acquia_migrate\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @group acquia_migrate
 * @group acquia_migrate__core
 */
class ReinstallationExperienceTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'acquia_migrate',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Checks the experience of (re)installing Acquia Migrate Accelerate.
   */
  public function testReInstallationExperience() {
    $assert_session = $this->assertSession();
    // The frontpage should contain a link to configure user 1.
    $this->drupalGet('');
    $assert_session->linkExists('Essential configuration');
    $assert_session->linkNotExists('Log in');
    $assert_session->pageTextContains('Import your content');

    // Clicking that link should allow configuring user 1.
    $this->clickLink('Essential configuration');
    $assert_session->elementExists('css', 'form[data-drupal-selector="user-acquia-migrate-configure-user-one-form"]');
    $input = [
      'source_site_info[base_url]' => 'https://example.com',
      'mail' => 'john.doe@example.com',
      'name' => 'john.doe',
      'pass[pass1]' => 'https://xkcd.com/936/',
      'pass[pass2]' => 'https://xkcd.com/936/',
    ];

    // Saving that form should redirect us to the original page, now without the
    // link we previously clicked, but with a link to log in instead.
    $this->submitForm($input, 'Save');
    $assert_session->addressEquals('/acquia-migrate-accelerate/get-started');
    $assert_session->linkNotExists('Configure user 1');
    $assert_session->linkExists('Log in');
    $assert_session->pageTextContains('Import your content');

    // Clicking that link should allow logging in.
    $this->clickLink('Log in');
    $assert_session->elementExists('css', 'form[data-drupal-selector="user-login-form"]');
    $input = [
      'name' => 'john.doe',
      'pass' => 'https://xkcd.com/936/',
    ];

    // Logging in should redirect us once again to the original page.
    $this->submitForm($input, 'Log in');
    $assert_session->addressEquals('/acquia-migrate-accelerate/get-started');
    $assert_session->linkNotExists('Configure user 1');
    $assert_session->linkNotExists('Log in');
    $assert_session->pageTextContains('Import your content');

    // Log out and reinstall the module.
    $this->drupalLogout();
    $this->container->get('module_installer')->uninstall(['acquia_migrate']);
    $this->container->get('module_installer')->install(['acquia_migrate']);

    // The frontpage should NOT contain a link to configure user 1.
    $this->drupalGet('');
    $assert_session->linkNotExists('Configure user 1');
    $assert_session->linkExists('Log in');
    $assert_session->pageTextContains('Import your content');
  }

}
