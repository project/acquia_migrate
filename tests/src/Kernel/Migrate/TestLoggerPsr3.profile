<?php

namespace Drupal\Tests\acquia_migrate\Kernel\Migrate;

use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;

/**
 * TestLogger for user migration test.
 */
class TestLogger implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * The log.
   *
   * @var mixed[][][]
   */
  protected $logs = [];

  /**
   * The state of this logger.
   *
   * @var bool
   */
  protected $active = FALSE;

  /**
   * Sets the state of the logger.
   *
   * @param bool $state
   *   The new state of the test logger.
   */
  public function setLogging(bool $state): void {
    $this->active = $state;
  }

  /**
   * Clears the log.
   */
  public function clearLog(): void {
    $this->logs = [];
  }

  /**
   * Returns the log.
   *
   * @return mixed[][][]
   *   The log entries grouped (and keyed by) their log level. An entry is an
   *   array of a message (string) and a context (which is an array).
   */
  public function getLog() {
    return $this->logs;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    if ($this->active) {
      $this->logs[$level][] = [
        'message' => $message,
        'context' => $context,
      ];
    }
  }

}
