module.exports = {
  sections: {
    sharedContentRow: {
      selector: '[id^="ef08b5721f28f83bc0f418fc5ae937a6"]',
      elements: {
        dropdownToggle: {
          selector: '.dropdown a',
        },
        dropdownMenu: {
          selector: '.dropdown-menu',
        },
        opComplete: {
          selector: '[title$="completed"]',
        },
      },
    },
    usersRow: {
      selector: '[id^="dbdd6377389228728e6ab594c50ad011"]',
      elements: {
        dropdownToggle: {
          selector: '.dropdown a',
        },
        dropdownMenu: {
          selector: '.dropdown-menu',
        },
        opComplete: {
          selector: '[title$="completed"]',
        },
      },
    },
  },
};
