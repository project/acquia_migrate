import APIError from './api-error';

export default class ClientError extends APIError {}
