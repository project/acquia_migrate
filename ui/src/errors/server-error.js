import APIError from './api-error';

export default class ServerError extends APIError {}
